
import time
import requests
from selenium import webdriver
from bs4 import BeautifulSoup

def selenium():
	driver = webdriver.Chrome(executable_path='./chromedriver')
	time.sleep(5)
	while True:
		try:
			driver.get('https://www.mueller.at/p/nintendo-switch-joy-con-2er-set-neon-rot-neon-blau-2133110/')
			driver.find_element_by_class_name('mu-availability-box')
			time.sleep(30)
		except Exception:
			break
	time.sleep(1)
	add_to_cart = driver.find_element_by_class_name('add-to-cart')
	for x in range(10, 1, -1):
		try:
			driver.find_element_by_xpath("//select[@name='quantity']/option[text()='{}']".format(str(x))).click()
			break
		except Exception:
			pass
	add_to_cart.click()
	driver.get('https://www.mueller.at/basket/')
	time.sleep(3)
	driver.find_element_by_class_name('mu-basket2__proceed-button').click()
	time.sleep(3)	
	driver.find_element_by_class_name('mu-button2--full-width').click()

if __name__ == '__main__':
	selenium()
